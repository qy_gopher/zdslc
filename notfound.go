package main

import (
	"net/http"
	"text/template"
)

func notFound(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(404)
	t, err := template.ParseFiles("public/template/404.html", "public/template/head.html", "public/template/foot.html")
	if err != nil {
		panic(err)
	}
	t.Execute(w, nil)
}