package main

import "io/ioutil"

func sljs1() string {
	sljs, err := ioutil.ReadFile("public/sljs1.html")
	if err != nil {
		s := `
		<script>
			layer.open({
			type: 1,
			title: '正在收录中，请选择分类',
			closeBtn: 1,
			shadeClose: false,
			skin: 'yourclass',
			content: '<form action="/" method="POST"><input type="hidden" name="token" value="[token]"><input type="radio" name="class" value="其他网站">其他网站<input type="radio" name="class" value="目录导航">目录导航<input type="radio" name="class" value="在线工具">在线工具<br /><input type="radio" name="class" value="小说漫画">小说漫画<input type="radio" name="class" value="影音娱乐">影音娱乐<input type="radio" name="class" value="博客论坛">博客论坛<br /><input type="radio" name="class" value="资源网站">资源网站<input type="radio" name="class" value="网赚兼职">网赚兼职<input type="radio" name="class" value="主机空间">主机空间<br /><input type="submit" value="提交"></form>'
		  	});
		</script>`
		return s
	}
	return string(sljs)
}

func sljs2() string {
	sljs, err := ioutil.ReadFile("public/sljs2.html")
	if err != nil {
		s := `
		<script>
		var index = layer.load(1);
		layer.msg('正在收录中，请稍后', {
  			offset: '40%',
		});

		$.get("/query?url=[url]",function(data,status){	
			layer.close(index);
			window.location.href="https://www.zdslc.com/";
		});
		</script>`
		return s
	}
	return string(sljs)
}