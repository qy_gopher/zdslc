package main

import (
	"io/ioutil"
	"strings"
)

func mysqlConf() string {
	bytedata, err := ioutil.ReadFile("config.conf")
	if err != nil {
		panic(err)
	}
	data := string(bytedata)

	wz := strings.Index(data, "[mysql]") + 7
	data = data[wz:]

	data = strings.ReplaceAll(data, " ", "")
	data = strings.ReplaceAll(data, `=`, ``)
	data = strings.ReplaceAll(data, `"`, ``)
	data = strings.ReplaceAll(data, "\n", "")

	portwz := strings.Index(data, "port")
	databasewz := strings.Index(data, "database")
	userwz := strings.Index(data, "user")
	passwordwz := strings.Index(data, "password")

	host := data[4:portwz]
	port := data[portwz+4:databasewz]
	database := data[databasewz+8:userwz]
	user := data[userwz+4:passwordwz]
	password := data[passwordwz+8:]

	return user + ":" + password + "@tcp(" + host + ":" + port + ")/" + database
}

func portConf() string {
	bytedata, err := ioutil.ReadFile("config.conf")
	if err != nil {
		panic(err)
	}
	data := string(bytedata)

	wz1 := strings.Index(data, "[port]") + 7
	wz2 := strings.Index(data, "[mysql]")
	data = data[wz1:wz2]

	data = strings.ReplaceAll(data, " ", "")
	data = strings.ReplaceAll(data, `=`, ``)
	data = strings.ReplaceAll(data, `"`, ``)
	data = strings.ReplaceAll(data, "\n", "")

	portwz := strings.Index(data, "port") + 4

	return ":" + data[portwz:]
}