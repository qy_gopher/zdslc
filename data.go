package main

import (
	"context"
	"errors"
	"strings"
	"time"

	"github.com/go-rod/rod"
)

// "context"
// "fmt"
// "strings"

// "github.com/chromedp/chromedp"
// "github.com/go-rod/rod"
// "github.com/gogf/gf/frame/g"

func getData(url string) (string, stu) {
	m := domain(url)
	page := browser.MustPage()
	defer page.Close()
	wait := page.MustWaitNavigation()
	expage := page.MustNavigate(`https://seo.chinaz.com/` + m)
	wait()

	var data stu
	errString := getDataErr(expage, &data,
		`#site_title`, `#site_keywords`, `#site_description`,
		`body > div._chinaz-seo-new1.wrapper.mb10 > table > tbody > tr:nth-child(4) > td._chinaz-seo-newtc._chinaz-seo-newh40 > span:nth-child(1)`,
		`body > div._chinaz-seo-new1.wrapper.mb10 > table > tbody > tr:nth-child(2) > td._chinaz-seo-newtc._chinaz-seo-newh40.webrank > span.mr20 > a > i`,
		`body > div._chinaz-seo-new1.wrapper.mb10 > table > tbody > tr:nth-child(1) > td._chinaz-seo-newtc._chinaz-seo-newh78 > div._chinaz-seo-newrank > span.baidupcrank.mr20 > a > span`,
		`body > div._chinaz-seo-new1.wrapper.mb10 > table > tbody > tr:nth-child(1) > td._chinaz-seo-newtc._chinaz-seo-newh78 > div._chinaz-seo-newrank > span.baidumobilerank.mr20 > a > span`,
		`body > div._chinaz-seo-new1.wrapper.mb10 > table > tbody > tr:nth-child(1) > td._chinaz-seo-newtc._chinaz-seo-newh78 > div:nth-child(1) > span > i > a`,
		`#seo_BaiduSiteIndex_2`, `#seo_SogouPages`, `#seo_PagesSm`)

	return errString, data
}

func getDataErr(page *rod.Page, data *stu, sli ...string) string {
	var s []string
	var str string
	for _, element := range sli {
		err := rod.Try(func() {
			str = page.Timeout(3 * time.Second).MustElement(element).MustText()
		})
		if errors.Is(err, context.DeadlineExceeded) {
			go sendErr("getDataErr", err)
			return "超时"
		} else if err != nil {
			go sendErr("getDataErr", err)
			return "错误"
		}
		s = append(s, str)
	}

	data.title = s[0]
	data.keyword = s[1]
	data.description = s[2]
	data.icp = dataCut(s[3])
	data.alexa = s[4]
	data.bdll = dataCut(s[5])
	data.ydll = dataCut(s[6])
	data.qwll = s[7]
	data.bdsl = s[8]
	data.sgsl = s[9]
	data.smsl = s[10]

	return ""
}

func dataCut(str string) string {
	if strings.Contains(str, "备案号：") {
		return strings.Replace(str, "备案号：", "", 1)
	}
	if strings.Contains(str, "(抢注已过期备案域名)") {
		return strings.Replace(str, "(抢注已过期备案域名)", "", 1)
	}
	return strings.Replace(str, "估计流量：", "", 1)
}

// 获取数据版本2， 使用chromedp
// func getData1(url string) stu {
// 	ctx, cancel := chromedp.NewContext(context.Background())
// 	defer cancel()

// 	// var res string
// 	var data stu
// 	m := domain(url)
// 	err := chromedp.Run(ctx,
// 			chromedp.Navigate(`https://www.aizhan.com/cha/` + m),
// 			chromedp.Text(`#webpage_tdk_title`, &data.title, chromedp.ByID),
// 			chromedp.Text(`#webpage_tdk_keywords`, &data.keyword, chromedp.ByID),
// 			chromedp.Text(`#webpage_tdk_description`, &data.description, chromedp.ByID),
// 			chromedp.Text(`#icp_icp`, &data.icp, chromedp.ByID),
// 			chromedp.Text(`#alexa`, &data.alexa, chromedp.ByID),
// 			chromedp.Text(`#baidurank_ip`, &data.bdll, chromedp.ByID),
// 			chromedp.Text(`#baidurank_m_ip`, &data.ydll, chromedp.ByID),
// 	)
// 	if err != nil {
// 		panic(err)
// 	}

// 	// err = chromedp.Cancel(ctx)
// 	// if err != nil {
// 	// 	panic(err)
// 	// }

// 	return data

// }

// 获取数据版本1， 使用gf框架
// func zhuaqu(s string) map[string]string {
// 	str := `https://www.aizhan.com/cha/`

// 	i := strings.Index(s, `http`)
// 	if i == -1 {
// 		str += s
// 		s = `http://` + s
// 	} else {
// 		j := strings.Index(s, `://`) + 3
// 		str += s[j:]
// 	}
// 	fmt.Println("网址：", s)

// 	res := g.Client().HeaderRaw(`
// 	User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36
// 	`).GetContent(str)

// 	// wz := strings.Index(res, `<div id="webpage_title">`) + 24
// 	// wz2 := strings.Index(res, `<div class="title-entry">`) - 13
// 	// fmt.Println("网站标题：", res[wz:wz2])

// 	// wz = strings.Index(res, `<span id="baidurank_ip" class="red">`) + 36
// 	// wz2 = strings.Index(res, `<li>移动来路：<span id="baidurank_m_ip">`) - 23
// 	// fmt.Println("百度来路：", res[wz:wz2], "IP")

// 	// wz = strings.Index(res, `<td><span id="webpage_tdk_keywords">`) + 36
// 	// wz2 = strings.Index(res, `<td>一般不超过100字符</td>`) - 18
// 	// fmt.Println("网站关键词：", res[wz:wz2])

// 	// wz = strings.Index(res, `<td><span id="webpage_tdk_description">`) + 39
// 	// wz2 = strings.Index(res, `<td>一般不超过200字符</td>`) - 18
// 	// fmt.Println("网站描述：", res[wz:wz2])

// 	Map := map[string]string{
// 		"title": zqTitle(res),
// 		"description": zqDescribe(res),
// 		"keyword": zqKeyWord(res),
// 		"icp": zqIcp(res),
// 		"bdll": zqBdll(res),
// 		"ydll": zqYdll(res),
// 		// "alexa": zqAlexa(res),
// 	}

// 	return Map
// }

// func zqtitle(url string) string {
// 	str := `https://www.aizhan.com/cha/`

// 	i := strings.Index(url, `http`)
// 	if i == -1 {
// 		str += url
// 	} else {
// 		j := strings.Index(url, `://`) + 3
// 		str += url[j:]
// 		fmt.Println(url)
// 	}

// 	res := g.Client().HeaderRaw(`
// 	User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36
// 	`).GetContent(str)

// 	wz := strings.Index(res, `<div id="webpage_title">`) + 24
// 	if wz == -1 {
// 		fmt.Println("获取标题错误")
// 		return "获取标题错误"
// 	}
// 	wz2 := strings.Index(res, `<div class="title-entry">`) - 13

// 	fmt.Println(wz, wz2)

// 	return res[wz:wz2]
// }

// func zqTitle(res string) string {
// 	// res := zhuaqu(url)
// 	wz := strings.Index(res, `<div id="webpage_title">`) + 24
// 	wz2 := strings.Index(res, `<div class="title-entry">`) - 13
// 	return res[wz:wz2]
// }

// // func zqll(res string) string {
// // 	// res := zhuaqu(url)
// // 	wz := strings.Index(res, `<span id="baidurank_ip" class="red">`) + 36
// // 	wz2 := strings.Index(res, `<li>移动来路：<span id="baidurank_m_ip">`) - 23
// // 	return res[wz:wz2]
// // }

// func zqKeyWord(res string) string {
// 	// res := zhuaqu(url)
// 	wz := strings.Index(res, `<td><span id="webpage_tdk_keywords">`) + 36
// 	wz2 := strings.Index(res, `<td>一般不超过100字符</td>`) - 18
// 	return res[wz:wz2]
// }

// func zqDescribe(res string) string {
// 	// res := zhuaqu(url)
// 	wz := strings.Index(res, `<td><span id="webpage_tdk_description">`) + 39
// 	wz2 := strings.Index(res, `<td>一般不超过200字符</td>`) - 18
// 	return res[wz:wz2]
// }

// func zqIcp(res string) string {
// 	// res := zhuaqu(url)
// 	wz := strings.Index(res, `id="icp_icp"`) + 13
// 	wz2 := strings.Index(res, `<li>性质：<span id="icp_type">`) - 17
// 	return res[wz:wz2]

// }

// func zqBdll(res string) string {
// 	wz := strings.Index(res, `<span id="baidurank_ip" class="red">`) + 36
// 	wz2 := strings.Index(res, `<li>移动来路：<span id="baidurank_m_ip">`) -23
// 	return res[wz:wz2]
// }

// func zqYdll(res string) string {
// 	wz := strings.Index(res, `<span id="baidurank_m_ip">`) + 26
// 	wz2 := strings.Index(res, `<li>出站链接：<span id="webpage_link_o">`) - 23
// 	return res[wz:wz2]
// }

// func zqAlexa(res string) string {
// 	wz := strings.Index(res, `<ul id="alexa">`) + 34
// 	wz2 := strings.Index(res, `<td class="thead">备案信息</td>`) - 60
// 	return res[wz:wz2]
// }
