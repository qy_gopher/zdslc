package main

import (
	"strings"
)

func cutURL(url string) string {
	url = strings.Replace(url, "://", "[]", 1)
	wz := strings.Index(url, "/")
	if wz == -1 {
		url = strings.Replace(url, "[]", "://", 1)
		return url + "/"
	}
	url = strings.Replace(url[:wz + 1], "[]", "://", 1)
	return url
}