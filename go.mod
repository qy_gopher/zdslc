module zdslc

go 1.15

require (
	github.com/go-rod/rod v0.96.1
	github.com/go-sql-driver/mysql v1.5.0
)
