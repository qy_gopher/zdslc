package main

import (
	"crypto/md5"
	"fmt"
	"net/http"
	"strings"
	"text/template"
)

// web主页
func index(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		notFound(w, r)
		return
	}
	cz := strings.Index(r.UserAgent(), `MSIE 9.0`)
	if cz != -1 {
		fmt.Fprint(w, "错误")
		return
	}

	url := r.Referer()
	t, err := template.New("index.html").Funcs(template.FuncMap{"add": Add}).ParseFiles("public/template/index.html", "public/template/head.html", "public/template/foot.html")
	// t, err := template.ParseFiles("index.html")
	if err != nil {
		sendErr("index", err)
		fmt.Fprint(w, "错误")
		return
	}

	tk := r.FormValue("token")
	if tk != "" {
		url := queryURL(`select url from tmp where token="` + tk + `"`)
		deleteURL(tk)
		if url == "0" || url == "-1" {
			t.Execute(w, querys())
			return
		}
		go insert(url, r.FormValue("class"))

		str := sljs2()
		str = strings.Replace(str, "[url]", url, 1)
		Data := querys()
		Data.Src = str
		t.Execute(w, Data)
		return
	}

	//设置模板左右标签为<% %>
	// t.Delims("<%", "%>")
	in := strings.Index(url, r.Host)
	if url == "" || in != -1 {
		// fmt.Println(url)
		//fmt.Fprint(w, sc)
		// Data := qurey()
		t.Execute(w, querys())
		return
	}
	jc := detectURL(url)
	if jc != 1 {
		return
	}
	url = cutURL(url)
	i := querycz(`select id from webinfo where url="` + url + `"`)
	if i == 0 {

		tk2 := queryToken(url)
		if tk2 != "0" {
			s := sljs1()
			s = strings.Replace(s, "[token]", tk2, 1)
	
			Data := querys()
			Data.Src = s
			t.Execute(w, Data)
			return
		}


		// a, insertdata := check(url)
		// if a != 0 {
		// 	Data := querys()
		// 	Data.Src = `
		// 	<script>
		// 	layer.open({
		// 	  type: 1,
		// 	  title: '提示',
		// 	  closeBtn: 1,
		// 	  shadeClose: false,
		// 	  skin: 'yourclass',
		// 	  content: '<br>检测到违规内容,无法自动收录<br>&nbsp;'
		// 	});
		//   </script>`
		//   t.Execute(w, Data)
		//   return
		// }


		// go insert(url)

		
		Md5 := md5.New()
		Md5.Write([]byte(url + "-站点收录池加密1"))
		token := fmt.Sprintf("%x", Md5.Sum(nil))
		insertURL(url, token)

		// str := `
		// <script>
		// var index = layer.load(1);
		// layer.msg('正在收录中，请稍后', {
  		// 	offset: '40%',
		// });

		// $.get("/query?url=[url]",function(data,status){	
    	// 	layer.close(index);
		// 	layer.open({
		// 	type: 1,
		// 	title: '收录成功，请选择分类',
		// 	closeBtn: 1,
		// 	shadeClose: false,
		// 	skin: 'yourclass',
		// 	content: '<form action="/tj/" method="GET"><input type="hidden" name="url" value="[url]"><input type="radio" name="class" value="其他网站">其他网站<input type="radio" name="class" value="目录导航">目录导航<input type="radio" name="class" value="在线工具">在线工具<br><input type="radio" name="class" value="小说影视">小说影视<input type="radio" name="class" value="博客论坛">博客论坛<input type="radio" name="class" value="资源网站">资源网站<br><input type="submit" value="提交"></form>'
		//   	});
		// });
		// </script>`

		s := `
		<script>
			layer.open({
			type: 1,
			title: '正在收录中，请选择分类',
			closeBtn: 1,
			shadeClose: false,
			skin: 'yourclass',
			content: '<form action="/" method="POST"><input type="hidden" name="token" value="[token]"><input type="radio" name="class" value="其他网站">其他网站<input type="radio" name="class" value="目录导航">目录导航<input type="radio" name="class" value="在线工具">在线工具<br><input type="radio" name="class" value="小说漫画">小说漫画<input type="radio" name="class" value="影音娱乐">影音娱乐<input type="radio" name="class" value="博客论坛">博客论坛<input type="radio" name="class" value="资源网站">资源网站<input type="radio" name="class" value="网赚兼职">网赚兼职<input type="radio" name="class" value="主机空间">主机空间<br><input type="submit" value="提交"></form>'
		  	});
		</script>`
		s = strings.Replace(s, "[token]", token, 1)

		Data := querys()
		Data.Src = s
		t.Execute(w, Data)
		
		return
	}
	if i == -1 {
		fmt.Fprint(w, "错误")
		return
	}
	//delete(url)
	//insert(url, i)
	update(url)
	// Data := query()
	t.Execute(w, querys())
}

// 网址转换为域名
/* func tfm(s string) string {
	wz := strings.Index(s, `://`) + 3
	s = s[wz:]
	wz2 := strings.Index(s, `/`)
	if wz2 == -1 {
		return s
	}
	return s[:wz2]
} */

//Add 模板函数
func Add(i int) int {
	return i + 1
}