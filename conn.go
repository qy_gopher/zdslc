package main

import (
	"database/sql"
	"fmt"
	"strconv"
	"strings"

	_ "github.com/go-sql-driver/mysql"
)

func conn() {
	db, _ = sql.Open("mysql", mysqlConf())
	err := db.Ping()
	if err != nil {
		fmt.Println("数据库连接失败:", err)
		return
	}
}

func querycz(s string) int {
	row := db.QueryRow(s)
	var id int
	err := row.Scan(&id)
	if err != nil {
		if err.Error() == "sql: no rows in result set" {
			// fmt.Println("不存在数据")
			return 0
		}
		fmt.Println("查询失败：", err)
		return -1
	}
	return 1
}

// queryURL 根据sql语句查询url
func queryURL(s string) string {
	row := db.QueryRow(s)
	var url string
	err := row.Scan(&url)
	if err != nil {
		if err.Error() == "sql: no rows in result set" {
			// fmt.Println("不存在数据")
			return "0"
		}
		fmt.Println("查询失败：", err)
		return "-1"
	}
	return url
}

func queryToken(url string) string {
	row := db.QueryRow(`select token from tmp where url="` + url + `"`)
	var token string
	err := row.Scan(&token)
	if err != nil {
		return "0"
	}
	return token
}

// func queryClass(s string) string {
// 	row := db.QueryRow(`select class from webinfo where html="` + s + `"`)
// 	var i int
// 	err := row.Scan(&i)
// 	if err != nil {
// 		panic(err)
// 	}
// 	return strconv.Itoa(i)
// }

// queryRowdata 根据html从数据库查询单个网站信息
func queryRowData(s string) wzdata {
	row := db.QueryRow(`select url,latest,title,keyword,description,icp,alexa,bdll,ydll,class from webinfo where html="` + s + `"`)
	var data wzdata
	err := row.Scan(&data.URL, &data.Date, &data.Title, &data.Keyword, &data.Description, &data.ICP, &data.Alexa, &data.Bdll, &data.Ydll, &data.Class)
	if err != nil {
		sendErr("queryRowData", err)
		var kdata wzdata
		return kdata
	}
	data.Domain = domain(data.URL)
	return data
}

func queryWebClass(class string) classdata {
	rows, err := db.Query(`select url,title,html,top from webinfo where class="` + class + `"`)
	if err != nil {
		sendErr("queryWebClass", err)
		var k classdata
		return k
	}
	var Data []data
	var stu data
	for rows.Next() {
		err := rows.Scan(&stu.URL, &stu.Title, &stu.HTML, &stu.Jf)
		if err != nil {
			sendErr("queryWebClass", err)
			var k classdata
			return k
		}
		Data = append(Data, stu)
	}

	rows, err = db.Query(`select url,title,html,top from webinfo where class="` + class + `" order by top desc limit 10`)
	if err != nil {
		sendErr("queryWebClass", err)
		var k classdata
		return k
	}
	var top []data
	for rows.Next() {
		err := rows.Scan(&stu.URL, &stu.Title, &stu.HTML, &stu.Jf)
		if err != nil {
			sendErr("queryWebClass", err)
			var k classdata
			return k
		}
		top = append(top, stu)
	}

	Datas := classdata{Data, top, class}

	return Datas
}

func queryLimit(s string) administerdatas {
	i, err := strconv.Atoi(s)
	if err != nil {
		panic(err)
	}
	rows, err := db.Query(`select url,title from webinfo where id between ` + strconv.Itoa(i-50) + ` and ` + s)
	if err != nil {
		panic(err)
	}
	var Administerdata []administerdata
	var stu administerdata
	for rows.Next() {
		err := rows.Scan(&stu.URL, &stu.Title)
		if err != nil {
			panic(err)
		}
		Administerdata = append(Administerdata, stu)
	}
	Administerdatas := administerdatas{Administerdata, i + 50}
	return Administerdatas
}

// func qurey1() {
// 	rows, err := db.Query(`select url, title from webinfo`)
// 	if err != nil {
// 		fmt.Println(1)
// 		panic(err)
// 	}
// 	stu := data{}
// 	sli := []data{}
// 	for rows.Next() {
// 		err = rows.Scan(&stu.URL, &stu.Title)
// 		if err != nil {
// 			fmt.Println("查询错误")
// 			panic(err)
// 		}
// 		sli = append(sli, stu)
// 	}
// 	lock.Lock()
// 	defer lock.Unlock()
// 	Data = Data[0:0]
// 	for i := len(sli) - 1; i >= 0; i-- {
// 		Data = append(Data, sli[i])
// 	}
// }

func querys() datas {
	// var Sort, Top, Dh, Tool []data
	// rows, err := db.Query(`select url,title,html,description from webinfo order by id desc limit 50`)
	// defer rows.Close()
	// if err != nil {
	// 	panic(err)
	// }
	// // var url string
	// // var title string
	// stu := data{}
	// for rows.Next() {
	// 	err = rows.Scan(&stu.URL, &stu.Title, &stu.HTML, &stu.Describe)
	// 	if err != nil {
	// 		fmt.Println("查询失败")
	// 		panic(err)
	// 	}
	// 	Sort = append(Sort, stu)
	// }

	// Data := datas{Sort, Top, Dh, Tool}

	// return Data

	sort := query(`select url,title,html,top from webinfo order by addup desc limit 21`)
	top := query(`select url,title,html,top from webinfo order by top desc limit 10`)
	latest := query(`select url,title,html,top,date from webinfo order by date desc limit 10`)
	other := query(`select url,title,html,top from webinfo where class="其他网站" limit 35`)
	dh := query(`select url,title,html,top from webinfo where class="目录导航" limit 35`)
	tool := query(`select url,title,html,top from webinfo where class="在线工具" limit 35`)
	nomh := query(`select url,title,html,top from webinfo where class="小说漫画" limit 35`)
	movmsc := query(`select url,title,html,top from webinfo where class="影音娱乐" limit 35`)
	blogbbs := query(`select url,title,html,top from webinfo where class="博客论坛" limit 35`)
	reswz := query(`select url,title,html,top from webinfo where class="资源网站" limit 35`)
	wzjz := query(`select url,title,html,top from webinfo where class="网赚兼职" limit 35`)
	idc := query(`select url,title,html,top from webinfo where class="主机空间" limit 35`)
	src := ""

	return datas{sort, top, latest, other, dh, tool, nomh, movmsc, blogbbs, reswz, wzjz, idc, src}
}

func insert(url, class string) {
	// url = cutURL(url)
	errString, data := getData(url)
	if errString != "" {
		ex := fmt.Sprintf("insert errdata set url=\"%v\", err=\"%v\"", url, errString)
		_, err := db.Exec(ex)
		if err != nil {
			sendErr("insert", err)
		}
		
		return
	}
	addup := queryAddup()
	id := queryID()
	html := strconv.Itoa(id) + ".html"
	// data := getData(url)
	// title := zqTitle(url)
	// keyword := zqKeyWord(url)
	// description := zqDescribe(url)
	stmt, err := db.Prepare(`insert webinfo set id=?,url=?,html=?,title=?,keyword=?,description=?,class=?,bdll=?,ydll=?,qwll=?,bdsl=?,sgsl=?,smsl=?,icp=?,alexa=?,addup=?`)
	if err != nil {
		sendErr("insert", err)
		return
	}

	_, err = stmt.Exec(id, url, html, data.title, data.keyword, data.description, class, data.bdll, data.ydll, data.qwll, data.bdsl, data.sgsl, data.smsl, data.icp, data.alexa, addup)
	if err != nil {
		sendErr("insert", err)
		return
	}
	defer stmt.Close()
	// id, err := res.LastInsertId()
	// if err != nil {
	// 	panic(err)
	// }
	// stmt.Close()
	// fileWrite(id)

	// 创建网站信息的html静态文件
	// htmlWrite(url, html)

	// fmt.Println("插入成功")
}

// insertURL 首次获得某网址referer后先暂存到tmp表，并添加token与之对应。
func insertURL(url, token string) {
	_, err := db.Exec(`insert tmp set url="` + url + `",token="` + token + `"`)
	if err != nil {
		sendErr("insertURL", err)
		return
	}
}

func update(m string) {
	addup := queryAddup()
	row := db.QueryRow(`select top from webinfo where url="` + m + `"`)
	var top int
	err := row.Scan(&top)
	if err != nil {
		sendErr("update", err)
		return
	}
	top++
	stmt, err := db.Prepare(`update webinfo set addup=?,top=? where url=?`)

	if err != nil {
		sendErr("update", err)
		return
	}
	defer stmt.Close()
	_, err = stmt.Exec(addup, top, m)
	if err != nil {
		sendErr("update", err)
		return
	}
}

func delete(m string) {
	stmt, err := db.Prepare(`delete from webinfo where url=?`)

	if err != nil {
		sendErr("delete", err)
		return
	}
	defer stmt.Close()
	_, err = stmt.Exec(m)
	if err != nil {
		sendErr("delete", err)
		return
	}
}

func deleteURL(token string) {
	_, err := db.Exec(`delete from tmp where token="` + token + `"`)
	if err != nil {
		sendErr("delete", err)
		return
	}
}

func query(s string) []data {
	var sli []data
	rows, err := db.Query(s)

	if err != nil {
		sendErr("query", err)
		var k []data
		return k
	}
	defer rows.Close()
	// var url string
	// var title string
	stu := data{}
	wz := strings.Index(s, "by date")
	if wz != -1 {
		for rows.Next() {
			err = rows.Scan(&stu.URL, &stu.Title, &stu.HTML, &stu.Jf, &stu.Date)
			if err != nil {
				fmt.Println("查询失败")
				sendErr("query", err)
				var k []data
				return k
			}
			sli = append(sli, stu)
		}
	} else {
		for rows.Next() {
			err = rows.Scan(&stu.URL, &stu.Title, &stu.HTML, &stu.Jf)
			if err != nil {
				fmt.Println("查询失败")
				sendErr("query", err)
				var k []data
				return k
			}
			sli = append(sli, stu)
		}
	}

	return sli
}

// func resetTop() {
// 	for {
// 		if time.Now().Hour() == 0 {
// 			_, err := db.Exec(`update webinfo set top=0`)
// 			if err != nil {
// 				panic(err)
// 			}
// 			fmt.Println("重置成功")
// 			time.Sleep(2 * time.Hour)
// 		}
// 	}
// }

func queryID() int {
	rows, err := db.Query(`select id from webinfo order by id desc limit 1`)

	if err != nil {
		sendErr("queryID", err)
		return 1
	}
	defer rows.Close()
	var id int
	for rows.Next() {
		err = rows.Scan(&id)
		if err != nil {
			sendErr("queryID", err)
			return 1
		}
	}
	return id + 1
}

func queryAddup() int64 {
	rows, err := db.Query(`select addup from webinfo order by addup desc limit 1`)

	if err != nil {
		sendErr("queryAddup", err)
		return 1
	}
	defer rows.Close()
	var addup int64
	for rows.Next() {
		err = rows.Scan(&addup)
		if err != nil {
			sendErr("queryAddup", err)
			return 1
		}
	}
	return addup + 1
}
