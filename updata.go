package main

import (
	"fmt"
	"strings"
	"time"
)

func wzupdate(html string) string {
	url := queryURL(`select url from webinfo where html="` + html + `"`)

	if url == "0" || url == "-1" {
		return "错误"
	}

	errString, data := getData(url)
	if errString != "" {
		return errString
	}
	date := getdate()

	stmt, err := db.Prepare(`update webinfo set latest=?,title=?,keyword=?,description=?,bdll=?,ydll=?,qwll=?,bdsl=?,sgsl=?,smsl=?,icp=?,alexa=? where url=?`)
	if err != nil {
		sendErr("wzupdate", err)
		return "错误"
	}
	defer stmt.Close()
	_, err = stmt.Exec(date, data.title, data.keyword, data.description, data.bdll, data.ydll, data.qwll, data.bdsl, data.sgsl, data.smsl, data.icp, data.alexa, url)
	if err != nil {
		sendErr("wzupdate", err)
		return "错误"
	}
	return "success"
}

func getdate() string {
	t := time.Now()
	str := fmt.Sprint(t)
	wz := strings.Index(str, ".")
	return str[:wz]
}