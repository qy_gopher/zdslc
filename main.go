package main

import (
	"database/sql"
	"fmt"
	"net/http"

	"github.com/go-rod/rod"
	// "sync"
	//"strconv"
	//_ "github.com/go-sql-driver/mysql"
)

type data struct {
	URL string
	Title string
	HTML string
	Jf int
	Date string
}

type datas struct {Sort, Top, Latest, Other, Dh, Tool, Nomh, Movmsc, Blogbbs, Reswz, Wzjz, IDC []data; Src string}

type classdata struct {
	Site []data
	Top []data
	Class string
}

type wzdata struct {
	URL string
	Domain string
	Date string
	Title string
	Keyword string
	Description string
	ICP string
	Bdll string
	Ydll string
	Alexa string
	Qwll string
	Bdsl string
	Sgsl string
	Smsl string
	Daycs int
	Moncs int
	Allcs int
	Class string
	Eqclass []data
}

type stu struct {
	title, keyword, description, icp, alexa, bdll, ydll, qwll, bdsl, sgsl, smsl string
}

type administerdata struct {
	URL, Title string
}

type administerdatas struct {
	Datas []administerdata
	Limit int
}

var h int

var browser = rod.New().MustConnect()

//Data 收录网站的信息
// var Data []data

var db *sql.DB

// var lock sync.Mutex

/* func conn1() error {
	db, _ = sql.Open("mysql", `root:a123456A@tcp(sh-cynosdbmysql-grp-gwh74n86.sql.tencentcdb.com:29395)/zdslc`)
	err := db.Ping()
	return err
}

func insert() (int64, error) {
	stmt, err := db.Prepare(`insert webinfo set ym=?,pm=?`)
	res, _ := stmt.Exec("s.python.ski", 2)
	id, _ := res.LastInsertId()
	return id, err
}

func queryRow(n int) {
	n1 := strconv.Itoa(n)
	str := `select ym from webinfo where id=` + n1
	rows := db.QueryRow(str)
	//var id int64
	var ym string
	//var pm int64
	err := rows.Scan(&ym)
	if err != nil {
		panic(err)
	}
	fmt.Println(ym)
} */

func main() {
	conn()
	// go resetTop()
	// query()

	http.HandleFunc("/", index)
	http.HandleFunc("/zd_admin/", admin)
	http.HandleFunc("/zd_login", login)
	http.HandleFunc("/site/", site)
	http.HandleFunc("/class/", class)

	// http.HandleFunc("/post/", webPost)

	// http.HandleFunc("/movmus/", movmus)
	// http.HandleFunc("/noca/", noca)
	// http.HandleFunc("/blogbbs/", blogbbs)
	// http.HandleFunc("/resweb/", resweb)
	// http.HandleFunc("/nav/", nav)
	// http.HandleFunc("/tool/", tool)
	// http.HandleFunc("/wzjz/", wzjz)
	// http.HandleFunc("/idc/", idc)
	// http.HandleFunc("/other/", other)
	http.HandleFunc("/query", querysl)
	fmt.Println("web服务创建成功")
	err := http.ListenAndServe(portConf(), nil)
	if err != nil {
		panic(err)
	}
}


