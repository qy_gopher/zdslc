package main

import (
	"strings"
)

func domain(url string) string {
	wz := strings.Index(url, "://") + 3
	m := url[wz:]
	wz = strings.Index(m, "/")
	if wz == -1 {
		return m
	}
	return m[:wz]
}