package main

import (
	"fmt"
	"html/template"
	"net/http"
	"strings"
)

func class(w http.ResponseWriter, r *http.Request) {
	path := r.URL.Path
	wz := strings.LastIndex(path, `/`)
	if wz > 9 {
		path = path[:wz]
	}
	var data classdata
	switch path {
		case "/class/movmus": 
			data = queryWebClass("影音娱乐")
		case "/class/noca":
			data = queryWebClass("小说漫画")
		case "/class/blogbbs":
			data = queryWebClass("博客论坛")
		case "/class/resweb":
			data = queryWebClass("资源网站")
		case "/class/nav":
			data = queryWebClass("目录导航")
		case "/class/tool":
			data = queryWebClass("在线工具")
		case "/class/wzjz":
			data = queryWebClass("网赚兼职")
		case "/class/idc":
			data = queryWebClass("主机空间")
		case "/class/other":
			data = queryWebClass("其他网站")
		default:
			notFound(w, r)
			return
	}
	t, err := template.New("webclass.html").Funcs(template.FuncMap{"add": Add}).ParseFiles("public/template/webclass.html", "public/template/head.html", "public/template/foot.html")
	if err != nil {
		sendErr("class", err)
		fmt.Fprint(w, "错误")
		return
	}
	t.Execute(w, data)
}