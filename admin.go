package main

import (
	"fmt"
	"html/template"
	"net/http"
)

func admin(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/zd_admin/" {
		notFound(w, r)
		return
	}


	cookie, err := r.Cookie("key")
	if err != nil {
		// fmt.Fprint(w, "获取Cookie错误")
		http.Redirect(w, r, "/zd_login", http.StatusFound)
		return
	}
	row := db.QueryRow(`select id from admin where password="` + cookie.Value + `"`)
	var id int
	err = row.Scan(&id)
	if err != nil {
		fmt.Fprint(w, `<html><body>错误，<a href="/zd_login">请点此重新登录</a></body></html>`)
		return
	}


	durl := r.FormValue("durl")
	if durl != "" {
		delete(durl)
		http.Redirect(w, r, "/zd_admin/", http.StatusFound)
		return
	}


	url := r.FormValue("url")
	class := r.FormValue("class")
	if url != "" && class != "" {
		go insert(url, class)
	}
	t, err := template.ParseFiles("public/template/admin.html")
	if err != nil {
		panic(err)
	}


	limit := r.FormValue("limit")
	if limit != "" {
		t.Execute(w, queryLimit(limit))
		return
	}
	
	t.Execute(w, queryLimit("0"))
}
