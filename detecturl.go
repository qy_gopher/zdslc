package main

import (
	"strings"
)

func detectURL(url string) int {
	wz := strings.Index(url, "http://")
	wz2 := strings.Index(url, "https://")
	if wz == 0 || wz2 == 0 {
		return 1
	}
	return -1
}