package main

import (
	"crypto/md5"
	"fmt"
	"net/http"
	"text/template"
)

func login(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/zd_login" {
		notFound(w, r)
		return
	}
	if r.Method != "POST" {
		t, err := template.ParseFiles("public/template/login.html")
		if err != nil {
			panic(err)
		}
		t.Execute(w, nil)
		return
	}
	password := r.FormValue("password")
	var user string
	Md5 := md5.New()
	Md5.Write([]byte(password + "站点收录池加密1"))
	pw := fmt.Sprintf("%x", Md5.Sum(nil))
	row := db.QueryRow(`select user from admin where password="` + pw + `"`)
	err := row.Scan(&user)
	if err != nil || user != r.FormValue("user") {
		fmt.Fprint(w, "账号或密码错误")
		return
	}
	cookie := http.Cookie {Name:"key", Value:pw, MaxAge: 360000}
	http.SetCookie(w, &cookie)
	http.Redirect(w, r, "/zd_admin/", http.StatusFound)
}